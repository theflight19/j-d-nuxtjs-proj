FROM node:14.5.0-alpine3.12

WORKDIR /home/node/app
COPY package.json /home/node/app
RUN npm install
COPY . /home/node/app

CMD ["npm", "run", "serve"]